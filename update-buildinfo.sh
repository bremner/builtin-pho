#!/bin/sh
# fetch new buildinfo files and index them

mkdir -p buildinfo/Debian

rsync -ai --exclude .nobackup \
      mirror.ftp-master.debian.org:/srv/ftp-master.debian.org/buildinfo/  \
      ./buildinfo/Debian | grep '^>f' | cut -c13- | python3 index-buildinfo.py -C ./buildinfo/Debian -F

