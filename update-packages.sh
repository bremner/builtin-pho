#!/usr/bin/bash

set -e

mkdir -p logs
YEAR="$(date -u +%Y)"
MONTH="$(date -u +%m)"
DAY="$(date -u +%d)"
LOG="logs/packages-$YEAR-$MONTH-$DAY.log"

fetch_xz_to_gz () {
    local url=$1
    local dest=$2

    wget -nv "$url" -a $LOG -O - | xz --decompress --stdout \
	| gzip -c > $dest
}

fetch_component () {
    local host=$1
    local dist=$2
    local component=$3

    mkdir -p dists/$dist/$component
    fetch_xz_to_gz https://$host/dists/$dist/$component/source/Sources.xz dists/$dist/$component/Sources.gz 2>>$LOG
    for arch in $(cat arches.txt); do
        if wget -a $LOG --spider https://$host/dists/$dist/$component/binary-$arch/Packages.xz; then
        	mkdir -p dists/$dist/$component/$arch
                fetch_xz_to_gz https://$host/dists/$dist/$component/binary-$arch/Packages.xz dists/$dist/$component/$arch/Packages.gz
        fi
    done
}

fetch_dist () {
    local host=$1
    local dist=$2
    local canfail=$3

    if wget -a $LOG --spider "https://$host/dists/${dist}"; then
	fetch_component $host $dist main
	fetch_component $host $dist non-free-firmware
    else
	if [ "$canfail" != "yes" ]; then
	    echo "Error missing dist $host $dist"
	    exit 1
	fi
    fi
}

for dist in $(cat dists.txt); do
    fetch_dist deb.debian.org/debian $dist no
    fetch_dist security.debian.org "${dist}-security" yes
    fetch_dist deb.debian.org/debian "${dist}-updates" yes
done

python3 index-packages.py
