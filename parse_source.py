# Parse the contents of a source field with optional version.  You'd
# think this would be a job for inheritance, but I'd need to inherit
# from more than one class.

import re

rex=re.compile('([^(\s]+)\s*[(]([^)]+)[)]$')

def parse_source(string):
    matches=rex.match(string)
    if matches:
        return (matches.group(1),matches.group(2))
    else:
        return (string,None)

def test_parse_source_without():
    assert parse_source('0xffff') == '0xffff'

def test_parse_source_with():
    assert parse_source('0xffff (0.8-1)') == ('0xffff','0.8-1')
