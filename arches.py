class Arches:
    RELEASE=[
        'all', 'amd64', 'arm64', 'armel', 'armhf',
        'i386', 'mips', 'mips64el', 'mipsel',
        'ppc64el', 's390x']

    COLUMNS = ",".join([ "arch_{:s} boolean".format(arch) for arch in RELEASE ])
    FORMAT = ",".join(['%s' for arch in RELEASE ])
