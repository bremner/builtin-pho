#!/bin/sh
# find source packages compiled against old automake
psql buildinfo <<EOF
select distinct p.source,p.version,d.version, b.path
from
      binary_packages p, builds b, depends d
where
      p.suite='sid' and b.source=p.source and
      b.arch_amd64 and p.arch = 'amd64'
      and p.version = b.version
      and d.id=b.id and d.depend='automake'
      and d.version < debversion '1:1.16.1'
EOF
