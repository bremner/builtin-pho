#!/bin/sh
# find binary packages with no buildinfo
psql buildinfo <<EOF
select distinct s.source,s.version
from
      source_packages s, build_depends b
where
      s.suite='sid'
      and s.id = b.id
      and b.depend = 'dh-elpa'
except
        select p.source,p.version
from binary_packages p, builds b
where
      b.source=p.source
      and p.source_version=b.source_version
EOF
