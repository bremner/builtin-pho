#!/bin/sh
# find source packages compiled against older version in a suite
# (e.g. to know which package needs to be NMUed in static languages such as golang)

Usage () {
  echo "Looks for source packages with binaries compiled against specific version or older" >&2
  echo >&2
  echo "Usage:" >&2
  echo "$0 <package> <version> [suite]" >&2
  echo "(suite defaults to sid)" >&2
  exit 2
}

package=$1;
version=$2;
suite=${3:-sid}

[ -z "$package" -o -z "$version" ] && Usage

psql buildinfo <<EOF
select distinct p.source,p.version,d.version as static_embeded_version
from
      binary_packages p, builds b, depends d
where
      p.suite='$suite' and b.source=p.source and
      b.arch_all and p.arch = 'all'
      and p.version = b.version
      and d.id=b.id
      and d.depend IN (select package from binary_packages where source = '$package')
      and d.version < debversion '$version';
EOF

