#!/bin/sh
psql buildinfo <<EOF
DROP TABLE source_packages CASCADE;
DROP TABLE binary_packages  CASCADE;
DROP TABLE builds CASCADE;
DROP TABLE depends CASCADE;
EOF
