#!/usr/bin/python3
from debian import deb822
from debian.deb822 import Sources, Packages
import sys
import psycopg2
import gzip
import argparse
from parse_source import parse_source
from arches import Arches

class BuildinfoDB:
    def __init__(self, verbose=0):
        self.verbose=verbose
        self.conn = psycopg2.connect('dbname=buildinfo')

    def begin(self):
        cursor = self.conn.cursor()
        cursor.execute('BEGIN')

    def commit(self):
        cursor = self.conn.cursor()
        cursor.execute('COMMIT')

    def done(self):
        self.conn.commit()
        self.conn.close()

    def reinit_sources(self):
        cursor = self.conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS source_packages
                (id SERIAL PRIMARY KEY, component text, suite text, source text, version debversion)''')
        cursor.execute('TRUNCATE TABLE source_packages CASCADE')

        cursor.execute('''CREATE TABLE IF NOT EXISTS build_depends
                                (id INTEGER REFERENCES source_packages(id),
                                depend text,
                                relation text,
                                version debversion,'''
                       +Arches.COLUMNS+')')
        cursor.execute('TRUNCATE TABLE build_depends')

    def reinit_binaries(self):
        cursor = self.conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS binary_packages
                (component text, suite text, arch debian_arch, source text NOT NULL, source_version debversion, package text, version debversion, sha256 text,PRIMARY KEY (suite,package,arch,version))''')
        cursor.execute('TRUNCATE TABLE binary_packages')

    def read_Sources(self, component, dist):
        cursor = self.conn.cursor()
        count=0
        if self.verbose > 1:
            print(f'trying {component}/{dist}')

        try:
            if self.verbose > 1:
                print(f'found dist {component}/{dist}')
            with gzip.open('dists/{:s}/{:s}/Sources.gz'.format(dist,component), mode='rb') as f:
                for src in Sources.iter_paragraphs(f):
                    count += 1
                    if (count % 1000) == 0:
                        if self.verbose > 0:
                            print('{:d} source packages processed for {:s}/{:s}'.format(count,component,dist))

                    package_name = src.get('Package')
                    version = src.get('Version')
                    parts = src.get('Section').split('/')
                    if len(parts) > 1:
                        component = parts[0]
                    else:
                        component = 'main'
                    cursor.execute("INSERT INTO source_packages VALUES (DEFAULT, %s,%s,%s,%s) RETURNING id",
                                           (component, dist, package_name, version))
                    # for use in build_depends
                    id = cursor.fetchone()[0]
                    for dep in src.relations['build-depends']:
                        restrict= {}
                        arches=[]
                        if dep[0]['arch']:
                            for pair in dep[0]['arch']:
                                restrict[pair.arch]=pair.enabled
                        for arch in Arches.RELEASE:
                            if arch in restrict:
                                arches.append(restrict[arch])
                            else:
                                arches.append(None)

                        rel = None
                        ver = None
                        if dep[0]['version']:
                            rel = dep[0]['version'][0]
                            ver = dep[0]['version'][1]
                        cursor.execute('INSERT INTO build_depends VALUES (%s,%s,%s,%s,'+Arches.FORMAT+')',
                                       (id, dep[0]['name'], rel, ver, *arches))
        except FileNotFoundError:
            if self.verbose > 1:
                print(f'dist {component}/{dist} not found')

    def read_Packages(self, component, dist, arch):
        cursor = self.conn.cursor()

        count=0
        try:
            with gzip.open('dists/{:s}/{:s}/{:s}/Packages.gz'.format(dist,component,arch), mode='rb') as f:
                for bin in Packages.iter_paragraphs(f):
                    count += 1
                    if (count % 1000) == 0:
                        if self.verbose > 0:
                            print('{:d} binary packages processed for {:s}/{:s}-{:s}'.format(count,component,dist,arch))
                    package_name = bin.get('Package')
                    source_text = bin.get('Source') or package_name
                    (source_name, source_version) = parse_source(source_text)
                    version = bin.get('Version')
                    source_version = source_version or version
                    SHA256 = bin.get('SHA256')
                    package_arch = bin.get('Architecture')
                    parts = bin.get('Section').split('/')
                    if len(parts) > 1:
                        component = parts[0]
                    else:
                        component = 'main'
                    if package_arch == 'all':
                        cursor.execute("INSERT INTO binary_packages VALUES (%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING",
                                       (component, dist, package_arch, source_name, source_version, package_name, version, SHA256))
                    else:
                        cursor.execute("INSERT INTO binary_packages VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
                                       (component,dist, package_arch, source_name, source_version, package_name, version, SHA256))
        except FileNotFoundError:
            if self.verbose > 1:
                print(f'dist-arch {component}/{dist}-{arch} not found')

parser = argparse.ArgumentParser()

parser.add_argument('--verbose', '-v', action='count', default=0)

args = parser.parse_args()

db = BuildinfoDB(verbose=args.verbose)
db.begin()
db.reinit_sources()
db.reinit_binaries()
with open("dists.txt") as dists:
    for dist in [ x.rstrip() for x in dists ]:
        for component in ['main', 'non-free-firmware']:
            for suite in [dist, f'{dist}-security', f'{dist}-updates']:
                db.read_Sources(component,suite)

        with open("arches.txt") as arches:
            for arch in [ x.rstrip() for x in arches ]:
                # arch all packages are included in the other Packages.gz files
                if arch != 'all':
                    for component in ['main', 'non-free-firmware']:
                        for suite in [dist, f'{dist}-security', f'{dist}-updates']:
                            db.read_Packages(component,suite,arch)
    db.commit()
    db.done()




